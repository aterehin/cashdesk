package com.example.aterehin.cashdesk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button cashDesk, products;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // CashDesk button
        cashDesk = findViewById(R.id.cashDesk);
        cashDesk.setOnClickListener(this);

        // Products button
        products = findViewById(R.id.products);
        products.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.cashDesk:
                changeActivity(CashDeskActivity.class);
                break;
            case R.id.products:
                changeActivity(ProductsActivity.class);
                break;
            default:
                break;
        }
    }

    private void changeActivity(Class activityClass){
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }
}
